# Virtual Quizzer

Software for quizzing remotely

## Overview of components

### Quizzer Hardware

* Quiz benches register jumps using switches connected to DIN cables
* A Raspberry Pi Zero reads jumps over GPIO
* The Pi, which is in USB gadget mode and connected to the computer that hosts the quizzer software, reports which quizzer jumped over USB serial

### Quizzer Software

* Plays back video of the quiz master asking the question
* Registers the timing of a jump relative to the start of playback
* Sends timing data to the quiz master
* Provides a real-time video call with the quiz master to negotiate the answer

### Quiz Master Software

* Records asking of the question and sends the question to quizzing teams
* Collects timing of jumps and identifies the team and quizzer to jump first
* Provides a real-time video call with the quizzer to negotiate the answer
* Records the score of each team

### Server Software

This is basically just a STUN or TURN server. Public STUN servers exist, but we may want to have our own (or at least a lengthy list of available options) in case the public option of choice goes down suddenly. If TURN is necessary (since it is not feasible to vet every quizzer and quiz master's networks we have no way of knowing, so we must assume that it will be), then we will have to host it ourselves. Maybe AWS?

**UPDATE:** We have decided to use XMPP, specifically [ejabberd](https://www.ejabberd.im/), as it solves many of our problems.

We could upload the quiz schedule to the server, give each team a login, and have the server put quizzers in the "room" they are supposed to be in.

## What Happens Before and During a Quiz

1. Quiz master records asking of questions
2. Two quizzer software instances and quiz master software establish a network connection using the STUN or TURN server
3. For each question:
  1. Quiz master software sends recording of question to each quizzer software
  2. Once transfer is complete, quizzer software plays back the question to the quizzers
  3. A quizzer jumps, and the timing of the jump is sent to the quiz master software
  4. The quiz master software indicates which quizzer jumped first in the asking of the question (may not be the same as jumped first according to wall time)
  5. The quiz master and both quizzer software instances begin a video call for the answer
  6. If a bonus question is needed then it occurs within the same video call


## What still has to be done

### Quizzer Hardware
- [x] Acquire prototype hardware
- [x] Configure USB gadget mode
- [ ] Write program to read GPIO and send signal over USB
- [ ] Investigate delay in signal propagation (RT kernel may be necessary)
- [ ] Standardize components and operating system image
- [ ] Create schematic and instructions for assembling hardware and obtaining software

### Software Backend
- [ ] Interface with quizzer hardware
- [x] Establish network connection with quizzers and quiz master (established via xmpp)
- [ ] Capture video and audio
- [ ] Support video call

### Quizzer Software Frontend
- [ ] Register for quiz and connect to quiz master
- [ ] Download and play video
- [ ] Display chart of benches that are "up" and "down" in green and red
- [ ] Display score
- [ ] Video call mode

### Quiz Master Software Frontend
- [ ] Register for quiz mastering and connect to quizzers
- [ ] Record and transmit video
- [ ] Record and display score
- [ ] Video call mode
- [ ] Enforce time limits for answers

### Server Software
- [x] Implement STUN
- [x] Implement TURN
- [x] Accept login ID/password for clients
- [ ] Mechanism to upload quiz schedule
- [ ] Web page to view quiz schedule
